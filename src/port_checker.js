import * as net from 'net';

export function checkPort(port) {
    return new Promise((resolve, reject) => {
        var server = net.createServer();

        server.on('error', e => reject(port));

        server.on('listening', e => {
            server.close();
            resolve(port);
        });

        server.listen(port);
    });
};

export function checkPortsRange(start=0, end=1) {
    return new Promise(resolve => {
        let counter = end - start + 1,
            openedPorts = [],
            resolver = () => {
                if (--counter === 0) {
                    resolve(openedPorts);
                }
            };

        for (let i = start; i <= end; i++) {
            checkPort(i)
                .then(port => {
                    openedPorts.push(port);
                    resolver();
                })
                .catch(resolver);
        }
    });
};