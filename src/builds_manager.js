'use strict';

import EventEmitter from 'events';
import * as cp from 'child_process';
import * as fs from 'fs';
import * as path from 'path';

export default class BuildsManager extends EventEmitter {
    constructor(betasFolder) {
        super();

        this._betasFolder = path.resolve(betasFolder);

        fs.lstat(this._betasFolder, (err, stat) => {
            if (err || !stat.isDirectory()) {
                fs.mkdir(this._betasFolder, err => console.log('Betas folder wasnt created', err));
            }
        });
    }

    create(betaInfo) {
        let localBetaInfo = Object.assign(betaInfo);

        // console.log('running build');
        let buildProcess = cp.spawn(
            '../build_scripts/build_beta.sh',
            [
                localBetaInfo.repoUrl,
                localBetaInfo.branchName,
                localBetaInfo.dirName,
                localBetaInfo.port
            ],
            {
                cwd: this._betasFolder
            }
        );

        buildProcess.stdout.on('data', data => {
            data
                .toString()
                .split('\n')
                .forEach(dataString => {
                    if (dataString.indexOf('SPID') > -1) {
                        localBetaInfo.spid = dataString.replace('SPID ', '');
                    }
                });

            // console.log(`data from building process \n ${data.toString()}`);

            this.emit('progress', localBetaInfo);
        });

        buildProcess.on('close', code => {
            code = Number(code);

            // console.log(`build stopped with code ${code}`);
            if (code > 0) {
                localBetaInfo.code = code; // TODO: Translate code to exception
                this.emit('fail', localBetaInfo);
            } else {
                this.emit('success', localBetaInfo);
            }
        });
    }

    restore(betaInfo) {
        console.log('restoring', betaInfo);
        let buildProcess = cp.spawn(
            '../build_scripts/restore_beta.sh',
            [
                betaInfo.dirName,
                betaInfo.port
            ],
            {
                cwd: this._betasFolder
            }
        );

        buildProcess.stdout.on('data', data => {
            data
                .toString()
                .split('\n')
                .forEach(dataString => {
                    if (dataString.indexOf('SPID') > -1) {
                        betaInfo.spid = dataString.replace('SPID ', '');
                    }
                });

            console.log(`data from restoring process \n ${data.toString()}`);

            this.emit('progress', betaInfo);
        });

        buildProcess.on('close', code => {
            code = Number(code);

            // console.log(`build stopped with code ${code}`);
            if (code > 0) {
                betaInfo.code = code; // TODO: Translate code to exception
                this.emit('fail', betaInfo);
            } else {
                this.emit('success', betaInfo);
            }
        });
    }

    remove(betaInfo) {
        console.log('Removing beta', betaInfo);
        let removingProcess = cp.spawn(
            '../build_scripts/remove_beta.sh',
            [
                betaInfo.dirName,
                betaInfo.spid
            ],
            {
                cwd: this._betasFolder
            }
        );

        // removingProcess.stdout.on('data', data => {
        //     console.log(`data from removing process \n ${data.toString()}`);
        // });

        removingProcess.on('close', code => {
            console.log(`Exited with code ${code}`);
            if (code) {
                console.log(`Beta wasnt removed. Process failed with code ${code}`);
            }

            this.emit('removed', betaInfo);
        });
    }
}