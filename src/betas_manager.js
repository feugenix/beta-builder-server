'use strict';

const STOP_PHRASE = 'branch';

import EventEmitter from 'events';
import * as fs from 'fs';
import * as crypto from 'crypto';
import {checkPort, checkPortsRange} from './port_checker';

export default class BetaManager extends EventEmitter {
    constructor(buildsManager, betasStorage, {minPort, maxPort}) {
        super();

        this._buildsManager = buildsManager;
        this._betas = betasStorage;
        this._availPorts = {
            minPort,
            maxPort
        };

        this
            .bindEvents()
            .restoreBetas();
    }

    getBranchNameFromUrl(url) {
        if (url.indexOf(STOP_PHRASE) === -1) {
            return;
        }

        return url.substr(url.indexOf(STOP_PHRASE) + STOP_PHRASE.length + 1);
    }

    getRepoFromUrl(url) {
        if (url.indexOf(STOP_PHRASE) === -1) {
            return;
        }

        return url.substr(0, url.indexOf(STOP_PHRASE));
    }

    getFreePort(ports) {
        let setOfPorts = new Set(ports);

        this._betas.betas.forEach(beta => setOfPorts.delete(beta.port));

        let freePorts = Array.from(setOfPorts);

        return freePorts[Math.floor(Math.random() * freePorts.length)];
    }

    bindEvents() {
        this._buildsManager.on('fail', betaInfo => {
            // console.log(`build failed with code ${betaInfo.code}`);
            this.emit('fail', 'Build failed', betaInfo);
        });

        this._buildsManager.on('success', betaInfo => {
            // console.log('build was successful');
            this._betas.add(betaInfo);
            betaInfo.status = 'complete';
            this.emit('success', betaInfo);
        });

        this._buildsManager.on('progress', betaInfo => {
            this.emit('progress', betaInfo);
        });

        this._buildsManager.on('removed', betaInfo => {
            this.emit('removed', betaInfo);
        });

        return this;
    }

    createNewBeta(betaInfo) {
        let branchName = this.getBranchNameFromUrl(betaInfo.gitUrl),
            hash = crypto.createHash('sha256');

        if (!branchName) {
            // console.log('no branch name');
            this.emit('fail', 'No branch name', betaInfo);
            return;
        }

        hash.update(Math.random() + betaInfo.gitUrl + betaInfo.title);
        let uid = hash.digest('hex');

        let newBetaInfo = {
            gitUrl: betaInfo.gitUrl,
            repoUrl: this.getRepoFromUrl(betaInfo.gitUrl),
            branchName,
            uid,
            dirName: branchName + uid,
            title: betaInfo.title,
            status: 'in process'
        };

        // console.log('checking ports');
        checkPortsRange(this._availPorts.minPort, this._availPorts.maxPort)
            .then(ports => {
                if (!ports.length) {
                    console.log('no free ports');
                    this.emit('fail', 'No free port', newBetaInfo);
                    return;
                }

                newBetaInfo.port = this.getFreePort(ports);

                this._buildsManager.create(newBetaInfo);
                this.emit('queued', newBetaInfo);
            });
    }

    restoreBetas() {
        this._betas.betas.forEach(beta => {
            beta.spid = undefined;
            beta.status = 'progress';

            this._buildsManager.restore(beta);
        });
    }

    updateBetaInfo(newInfo) {
        // TODO: remove beta, create beta
    }

    refreshBeta() {
        // TODO: stop server, pull from git, pip install, npm i, gulp
    }

    removeBeta(betaUid) {
        let removingBeta;

        this._betas.betas.forEach(beta => {
            if (beta.uid === betaUid) {
                removingBeta = beta;
            }
        });

        if (removingBeta) {
            this._buildsManager.remove(removingBeta);
            this._betas.remove(removingBeta);
        } else {
            this.emit('fail', 'Cant remove beta, cant find it', betaUid);
        }
    }
};