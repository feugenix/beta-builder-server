'use strict';

const PORT = 3000; // TODO: move to config
const BETAS_DIR = 'betas'; // TODO: move to config

import * as ws from 'ws';
import * as fs from 'fs';

import BetasStorage from './betas_storage';
import BetaManager from './betas_manager';
import BuildsManager from './builds_manager';
import {checkPort, checkPortsRange} from './port_checker';

let wss = new ws.Server({ port: PORT }),
    betasStorage = BetasStorage,
    betasManager = new BetaManager(
        new BuildsManager(BETAS_DIR),
        BetasStorage,
        {
            minPort: 40000, // TODO: move to config
            maxPort: 41000
        }
    );

let send = (socket, data) => socket.send(JSON.stringify(data)),
    broadcast = (wss, data) => wss.clients.forEach(client => client.send(JSON.stringify(data)));

betasManager.on('fail', (msg, betaInfo) => {
    // console.log('fail', msg, betaInfo);
    broadcast(wss, {
        type: 'fail',
        msg: msg,
        data: betaInfo
    });
});

betasManager.on('progress', betaInfo => {
    // console.log('progress', betaInfo);
    broadcast(wss, {
        type: 'progress',
        data: betaInfo
    });
});

betasManager.on('success', betaInfo => {
    // console.log('success', betaInfo);
    broadcast(wss, {
        type: 'success',
        data: betaInfo
    });
});

betasManager.on('removed', betaInfo => {
    broadcast(wss, {
        type: 'removed',
        data: betaInfo
    });
});

betasManager.on('queued', betaInfo => {
    // console.log('queued', betaInfo);
    broadcast(wss, {
        type: 'queued',
        data: betaInfo
    });
});

wss.on('connection', socket => {
    socket.on('message', msg => {
        let data = JSON.parse(msg),
            eventType = data.type,
            okRes = {
                type: 'notification',
                data: 'ok'
            };

        switch(eventType) {
            case 'list betas':

                let listBetasResponse = {
                    type: 'list betas',
                    data: betasStorage.betas
                };

                send(socket, listBetasResponse);
            break;

            case 'create beta':
                let newBeta = {
                    title: data.data.title,
                    gitUrl: data.data.gitUrl,
                    status: 'prepare'
                };

                betasManager.createNewBeta(newBeta);

                send(socket, okRes);
            break;

            case 'remove beta':
                betasManager.removeBeta(data.data);

                send(socket, okRes);
            break;

            default: console.log(`unsupported event ${eventType}`);
        }
    });
});

// console.log(`listen on ${PORT}`);