'use strict';

import * as fs from 'fs';
import * as path from 'path';

const BETAS_FILE_NAME = path.join(process.cwd(), 'betas.json'); // TODO: move to config

let betas;

try {
    betas = new Set(require(BETAS_FILE_NAME) || []);
} catch(e) {
    console.log('Cannot read betas file');
    betas = new Set();
    dumpBetas();
}

function dumpBetas() {
    fs.writeFile(BETAS_FILE_NAME, JSON.stringify(Array.from(betas)), e => {
        if (e) {
            console.log('Error saving betas to file', e);
        }
    });
}

export default class BetasStorage {
    static get betas() {
        return betas;
    }

    static add(beta) {
        betas.add(beta);
        dumpBetas();
    }

    static remove(removingBeta) {
        for (let beta of betas) {
            if (removingBeta.uid === beta.uid) {
                betas.delete(beta);
                dumpBetas();
                break;
            }
        }
    }
}