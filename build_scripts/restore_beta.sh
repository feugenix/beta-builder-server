#!/bin/sh

if [ $# -lt 2 ]
then
  echo "Too few args"
  exit 1
fi

DIR_NAME=$1
PORT=$2

cd $DIR_NAME
workon $DIR_NAME
./manage.py runserver 0.0.0.0:$PORT &>/dev/null & # TODO: handle server faults
echo "SPID $!"