#!/bin/sh

DIR_NAME=$1
SPID=$2

if [ $# -lt 2 ]
then
  echo "Too few args"
  exit 1
fi

echo "Killing process $SPID"
kill $SPID
echo "Removing virtual env"
rmvirtualenv $DIR_NAME
echo "Removing folder $DIR_NAME"
rm -rf $DIR_NAME