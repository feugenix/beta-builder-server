#!/bin/sh

if [ $# -lt 4 ]
then
  echo "Too few args"
  exit 1
fi

REPO_LINK=$1
BRANCH_NAME=$2
DIR_NAME=$3
PORT=$4

echo "Cloning branch $BRANCH_NAME from $REPO_LINK into $DIR_NAME"
git clone $REPO_LINK --branch=$BRANCH_NAME --depth=1 $DIR_NAME
echo "Moving to $DIR_NAME"
cd $DIR_NAME
rm -rf .git
echo "Creating virtual env $DIR_NAME"
mkvirtualenv $DIR_NAME

cp ../settings.local .

echo "Installing pip packages"
pip install -r requirements.txt
echo "Installing npm packages"
npm i
echo "Running gulp"
gulp
echo "Building translations"
./manage.py make_javascript_translations -a
echo "Running server"
./manage.py runserver 0.0.0.0:$PORT &>/dev/null & # TODO: handle server faults
echo "SPID $!"